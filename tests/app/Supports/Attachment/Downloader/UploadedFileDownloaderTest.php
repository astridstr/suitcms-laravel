<?php

namespace TestApp\Supports\Attachment\Downloader;

use App\Supports\Attachment\Downloader\UploadedFileDownloader;
use Illuminate\Contracts\Filesystem\Filesystem;
use Mockery;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TestApp\TestCase;

class UploadedFileDownloaderTest extends TestCase
{
    protected $filesystem;

    protected $downloader;

    public function setUp()
    {
        parent::setUp();

        $filesystem = Mockery::mock(Filesystem::class);
        $extensionGuesser = new MimeTypeExtensionGuesser;

        $downloader = new UploadedFileDownloader($filesystem, $extensionGuesser);

        $this->filesystem = $filesystem;
        $this->downloader = $downloader;
    }

    public function testDownload()
    {
        $uploadedFile = Mockery::mock(UploadedFile::class);
        $uploadedFile->shouldReceive('getClientOriginalName')->andReturn('filename.ext');
        $uploadedFile->shouldReceive('getRealPath')->andReturn('/path/to/filename.ext');
        $uploadedFile->shouldReceive('getClientMimeType')->andReturn('mime');
        $uploadedFile->shouldReceive('getClientSize')->andReturn(1000);

        $this->filesystem->shouldReceive('put')->once();

        $attachment = $this->downloader->download($uploadedFile);

        $this->assertEquals('1000', $attachment->getSize());
        $this->assertEquals('mime', $attachment->getMimeType());
        $this->assertRegExp('/[0-9]{4}\/[A-Za-z]{3}\/[0-9]{2}\/[a-z0-9]{13}\/filename\.ext/', $attachment->getUrlPath());
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadNotFound()
    {
        $uploadedFile = Mockery::mock(UploadedFile::class);
        $uploadedFile->shouldReceive('getClientOriginalName')->andReturn('filename.ext');
        $uploadedFile->shouldReceive('getClientMimeType')->andReturn('image/jpeg');
        $uploadedFile->shouldReceive('getRealPath')->andReturn(false);

        $this->filesystem->shouldReceive('put')->never();

        $this->downloader->download($uploadedFile);
    }

    public function testValidate()
    {
        $this->assertTrue($this->downloader->validate(Mockery::mock(UploadedFile::class)));
    }

    public function testValidateNotValid()
    {
        $this->assertFalse($this->downloader->validate(Mockery::mock(Mockery::class)));
        $this->assertFalse($this->downloader->validate(1));
        $this->assertFalse($this->downloader->validate('path/to/file'));
    }
}
