<?php

namespace App\Supports\Attachment;

use Exception;

class ResourceNotFoundException extends Exception
{
}
