<?php

namespace App\Supports;

use App\Http\Form\Admin\BackendLoginForm as Form;
use Illuminate\Auth\AuthManager as Auth;

class BackendSessionProcess
{

    protected $user;

    protected $form;

    protected $auth;

    public function __construct(Form $form, Auth $auth)
    {
        $this->auth = $auth;
        $this->form = $form;
    }

    public function login()
    {
        $this->form->filterInput('strtolower', ['username', 'captcha']);
        $this->form->filterInput('trim', ['username', 'captcha']);
        if (!$this->form->validate() ||
            !$this->authenticate($this->getCredentials())
        ) {
            if (!empty($this->form->errors()) && $this->form->errors()->has('captcha')) {
                \Session::flash(NOTIF_DANGER, $this->form->errors()->first('captcha'));
            } else {
                \Session::flash(NOTIF_DANGER, suitTrans('messages.login_fail'));
            }
            return false;
        }

        $this->auth->user()->login();

        return true;
    }

    public function logout()
    {
        $user = $this->auth->user();
        $this->auth->logout();

        $user->logout();
        \Session::flash(NOTIF_SUCCESS, suitTrans('messages.logout_success'));
    }

    protected function getCredentials()
    {
        $credentials = [];
        $credential = $this->form->getCredential();
        $credential['active'] = true;

        $credentials[] = $credential;

        $credential['email'] = $credential['username'];
        unset($credential['username']);

        $credentials[] = $credential;

        return $credentials;
    }

    protected function authenticate($credentials)
    {
        foreach ($credentials as $credential) {
            if ($this->auth->attempt($credential)) {
                \Session::flash(
                    NOTIF_SUCCESS,
                    suitTrans('messages.login_success', ['name' => $this->auth->user()->getName()])
                );
                return true;
            }
        }
        return false;
    }
}
