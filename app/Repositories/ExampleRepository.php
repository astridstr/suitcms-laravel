<?php

namespace App\Repositories;

use App\Model\User;
use App\Repositories\Extension\CacheableTrait;

class ExampleRepository
{
    use CacheableTrait;

    protected function getData($now)
    {
        $data = User::whereDate('created_at', '>', $now)->get();
        return $data;
    }
}
