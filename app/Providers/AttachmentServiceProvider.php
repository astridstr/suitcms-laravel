<?php

namespace App\Providers;

use App\Supports\Attachment\Manager;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesserInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;

class AttachmentServiceProvider extends ServiceProvider
{
    protected $downloaders = [
        \App\Supports\Attachment\Downloader\UploadedFileDownloader::class,
        \App\Supports\Attachment\Downloader\YoutubeDownloader::class,
        \App\Supports\Attachment\Downloader\UrlDownloader::class,
        \App\Supports\Attachment\Downloader\LocalDownloader::class
    ];

    public function boot()
    {
        // pass
    }

    public function register()
    {
        $downloaderClasses = $this->downloaders;
        $this->app->singleton(ExtensionGuesserInterface::class, MimeTypeExtensionGuesser::class);
        $this->app->singleton('attachment.manager', function ($app) use ($downloaderClasses) {
            $downloaders = [];
            foreach ($downloaderClasses as $class) {
                $downloaders[] = $app->make($class);
            }

            return new Manager($downloaders);
        });
    }
}
