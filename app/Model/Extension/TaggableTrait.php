<?php
namespace App\Model\Extension;

use App\Model\Tag;

trait TaggableTrait
{

    /**
     * Coma-separated tag
     * @var string
     */
    protected $tagsAttribute;

    /**
     * TaggableTrait boot trait.
     * @return void
     */
    public static function bootTaggableTrait()
    {
        static::deleting(function ($model) {
            $model->tags()->sync([]);
        });

        static::saved(function ($model) {
            if ($model->tagsAttribute !== null) {
                $model->saveTags($model->tagsAttribute);
            }
        });
    }

    /**
     * Set Tag attributes with coma-separated value.
     * @param string $value
     */
    public function setTagsAttribute($value)
    {
        $this->tagsAttribute = strtolower($value);
    }

    /**
     * Define many to many relation to Tag
     *
     * @return Illuminate\Database\Eloquent\MorphToMany;
     */
    public function tags()
    {
        return $this->morphToMany(\App\Model\Tag::class, 'taggable');
    }

    /**
     * Save Tags to storage
     *
     * @param array | string $data
     * @return array
     *
     * @throws InvalidArgumentException
     */
    public function saveTags($data)
    {
        if (is_string($data)) {
            $data = explode(',', $data);
        }

        if (!is_array($data)) {
            throw new \InvalidArgumentException(
                'saveTags function only accept array or string. Input was ' . gettype($data)
            );
        }

        // trim each tag
        $data = array_map('trim', $data);

        $tagLists = Tag::getTagsLists($data)->all();

        $tagIds = [];
        foreach (array_diff($data, $tagLists) as $value) {
            if ($value === '') {
                continue;
            }
            $tag = Tag::create(['name' => $value]);
            $tagIds[] = $tag->getKey();
        }

        return $this->tags()->sync(array_merge($tagIds, array_keys($tagLists)));
    }

    /**
     * Get list of tags in string
     *
     * @return string
     */
    public function getStringTags($delimiter = ',')
    {
        return implode($delimiter, $this->tags->lists('name')->all());
    }
}
