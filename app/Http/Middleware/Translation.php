<?php
namespace App\Http\Middleware;

use Closure;

class Translation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sessionLang = session()->get('site.locale');
        if (empty($sessionLang)) {
            $sessionLang = config()->get('app.fallback_locale');
        }
        $lang = request()->get('locale', $sessionLang);
        if (isAcceptLang($lang)) {
            session()->put('site.locale', $lang);
            app()->setLocale($lang);
        }
        return $next($request);
    }
}
