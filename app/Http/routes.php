<?php

use Suitcms\Model\Menu;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('menu', ['menus' => App\Model\Menu::getAllMenu()]);
});

Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
    Route::resource('users', 'UserController', ['only' => ['show', 'index']]);
    Route::resource('menus', 'MenuController', ['only' => ['show', 'index']]);
    Route::resource('settings', 'SettingController', ['only' => ['show', 'index']]);
    Route::resource('pages', 'PageController', ['only' => ['show', 'index']]);
});

Route::group(['prefix' => \I18n::routePrefix()], function () {
    Route::get('{url}', ['uses' => 'PageController@show'])->where('url', '.+');
});

Route::get('{url}', ['uses' => 'I18nController@handle'])->where('url', '.+')->middleware('negotiate.language');
