<?php
namespace App\Http\Controllers\Api;

use App\Model\Page;
use Illuminate\Http\Request;

class PageController extends ResourceController
{
    public function __construct(Page $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with('attachments');
    }
}
