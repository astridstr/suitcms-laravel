<?php

namespace App\Http\Controllers\Api;

use App\Model\User;

class UserController extends ResourceController
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
