<?php

namespace App\Http\Controllers\Api;

use App\Model\Menu;

class MenuController extends ResourceController
{
    public function __construct(Menu $model)
    {
        parent::__construct($model);
    }
}
