<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class I18nController extends Controller
{
    public function handle(Request $request)
    {
        return redirect(\I18n::url($request->getRequestUri()));
    }
}
