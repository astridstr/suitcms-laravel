<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class RemindersController extends BaseController
{
    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind()
    {
        config()->set('auth.password.email', suitViewName('emails.auth.reminder'));
        $response = \Password::sendResetLink(\Input::only('email'), function ($message) {
            $message->subject(config()->get('suitcms.site_name') . ' Password Reminder');
        });
        switch ($response) {
            case \Password::INVALID_USER:
                return \Redirect::back()->with(NOTIF_DANGER, suitTrans($response))->with('state-remind', true);

            case \Password::RESET_LINK_SENT:
                return \Redirect::route(suitRouteName('login'))->with(NOTIF_SUCCESS, suitTrans($response));
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset($token = null)
    {
        if ($token === null) {
            throw new NotFoundHttpException;
        }

        return \View::make(suitViewName('password.reset'))->with('token', $token);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset(Request $request)
    {
        $credentials = \Input::only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|basic_password|confirmed',
            'token' => 'required'
        ]);

        $response = \Password::reset($credentials, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        switch ($response) {
            case \Password::INVALID_PASSWORD:
            case \Password::INVALID_TOKEN:
            case \Password::INVALID_USER:
                return \Redirect::back()->with(NOTIF_DANGER, suitTrans($response));

            case \Password::PASSWORD_RESET:
                return \Redirect::to(suitLoginUrl())->with(NOTIF_SUCCESS, suitTrans($response));
        }
    }
}
