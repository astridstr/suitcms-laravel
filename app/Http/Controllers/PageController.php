<?php
namespace App\Http\Controllers;

use App\Model\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    public function show(Request $request, $url)
    {
        $urlPrefix = $this->getPrefix($url);
        $slug = $this->slug($url);

        $page = $this->page->where('url_prefix', $urlPrefix)->findOrFailByUrlKey($slug);
        $layout = app($page->layout);

        view()->share([
            'metaTitle' => $page->title,
            'metaDescription' => $page->description
        ]);

        return $layout->handle($request, $page);
    }

    protected function getPrefix($url)
    {
        $prefix = dirname($url);
        return trim($prefix, '.');
    }

    protected function slug($url)
    {
        return basename($url);
    }
}
