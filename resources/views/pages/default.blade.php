@extends('_layouts.web')

@section('site-content')
Default
<ul>
    <li>{{ $page->title }}</li>
    <li>{{{ $page->description}}}</li>
    <li>{{{ $page->content}}}</li>
    <li>{{ $page->updated_at}}</li>
    <li>{{ $page->created_at }}</li>
</ul>
@endsection
