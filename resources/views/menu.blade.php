@extends('_layouts.web')

@section('site-content')
<ul>
    @foreach($menus as $menu)
    <li>
        {{ $menu->title }} - <a href="{{$menu->real_url}}">{{$menu->real_url}}</a>
    </li>
    @endforeach
</ul>
@endsection
