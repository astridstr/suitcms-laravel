@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute($routePrefix.'.create') }}" class="btn btn-sm btn-primary">
            <i class="glyphicon glyphicon-plus"></i> Add New
        </a>
    </div>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="username">Username</label>
    <label><input type="checkbox" data-name="email">Email</label>
    <label><input type="checkbox" checked data-name="name">Name</label>
    <label><input type="checkbox" checked data-name="group_type">Group</label>
    <label><input type="checkbox" checked data-name="active">Active</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
