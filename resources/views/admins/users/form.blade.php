@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('users.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitSelect('group_type', 'Group', $groups) !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitText('username', 'Username') !!}
        {!! Form::suitText('email', 'Email') !!}
        {!! Form::suitPassword('password', 'Password') !!}
        {!! Form::suitPassword('password_confirmation', 'Password Confirmation') !!}
        {!! Form::suitText('name', 'Name') !!}
        {!! Form::suitTextarea('about', 'About') !!}
        {!! Form::suitSelect('active', 'Active', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
