[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

# Database

Here are SuitCMS base database tables

1. **users**
1. **tags**
1. **taggables**
1. **menus**
1. **categories**
1. **pages**
1. **page_category**
1. **model_log**
1. **model_log_data**

## **users**

| Field | Type | Description | Other |
| ------| ---- | ----------- | ----- |
| id | INT | ID | primary |
| group_type | TINYINT | Group of the user | index |
| username | VARCHAR(255) | Username, used for log in | unique |
| email | VARCHAR(255) | Email, used for log in | unique |
| password | VARCHAR(255) | Password, use bcrypt | - |
| slug | VARCHAR(255) | slug used for url key | unique |
| name | VARCHAR(255) | Name | default: Anonymous |
| about | TEXT | Description about user | NULL |
| date_of_birth | DATETIME | date of birth | NULL |
| active | TINYINT | Active or Not | default: 1 |

[Next: Model](https://gitlab.com/suitmedia/suitcms/wikis/Model)
