[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

**Tidak terbaru**

# Main Component
* Laravel 5
* Metronic 3.5

# Package Dependencies
* illuminate/html 5.0.0
* wicochandra/captcha 1.2.1
* guzzlehttp/guzzle 5.2.0
* barryvdh/laravel-debugbar 2.0.1
* barryvdh/laravel-elfinder 0.3.3

# Requirement
* PHP >= 5.4
* MySQL >= 5.5

[Next: Features](https://gitlab.com/suitmedia/suitcms/wikis/Feature)
