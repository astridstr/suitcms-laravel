# SuitCMS

Platform for standard web content management system based on laravel, developed by Suitmedia

![SuitCMS](http://i.imgur.com/V6zSX.png)

Content :

1. [Requirement](https://gitlab.com/suitmedia/suitcms/wikis/Requirement)
1. Features
1. [Installation](https://gitlab.com/suitmedia/suitcms/wikis/Installation)
1. [Configuration](https://gitlab.com/suitmedia/suitcms/wikis/Configuration)
1. [Database](https://gitlab.com/suitmedia/suitcms/wikis/Database)
1. [Model](https://gitlab.com/suitmedia/suitcms/wikis/Model)
    1. BaseModel
    1. Trait
        1. OrderableTrait
        1. PublishableTrait
        1. SluggableTrait
        1. TaggableTrait
        1. TreeTrait
        1. AttachableTrait
1. [BaseForm](https://gitlab.com/suitmedia/suitcms/wikis/BaseForm)
    1. Defining Rules
    1. Filtering
1. [ResourceController](https://gitlab.com/suitmedia/suitcms/wikis/ResourceController)
    1. How to Extends
    1. Convention
    1. Controller Action Method Flow
    1. Passing Form Data to View
    1. Passing Data to BaseForm
    1. Save Request Data
1. [View Structure](https://gitlab.com/suitmedia/suitcms/wikis/ViewStructure)
1. Site Settings
1. Commands
1. [ Controller Trait](https://gitlab.com/suitmedia/suitcms/wikis/controller-trait)
    1. PublishToggleTrait
