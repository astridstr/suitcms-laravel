[Back To Home](https://gitlab.com/suitmedia/suitcms/wikis/Home)

# Model

## BaseModel (`App\Model\BaseModel`)

The goal of this class are as main root model and to make model can be access uniquely for url using other key (eq `slug`) in addition to primary key which is always `id`. As Laravel 5, there are `getRouteKey()` and `getRouteKeyName()`methods which are can be used to accomplish one of this class's goal if used with the help of [Route Model Binding](http://laravel.com/docs/5.0/routing#route-model-binding).

To define which field used for url key, in the Child Model must overide `$url_key` attributes.

```
#!php
<?php
class NewModel extends BaseModel {

    protected $url_key = 'slug';
}
?>
```

BaseModel class also override `getRouteKeyName()` to use `$url_key`. So too get the url key value can use either `getUrlKey()` or `getRouteKey()`. To find using this key on query builder,

```
#!php
<?php
    Model::findByUrlKey('hello-world');
    Model::findOrFailUrlKey('hello-world');

    // or combined with other condition
    Model::whereNotNull('published_at')->findByUrlKey('hello-world');
?>
```

BaseModel class also has defined some scope that can be used for all model.
```
#!php
<?php
    Model::random(10)->get()        // to obtain random order
    Model::newestCreated()->get()   // to obtain data order by its created_at attribute
?>
```

## Traits

Suitcms also provides some traits which considered to be used on many model. To read

### OrderableTrait (`App\Model\Extension\OrderableTrait`)

The model must have `order` field.

```
#!php
<?php
    Model::asc()->get()     // order by ASC on `order` field
    Model::desc()->get()    // order by DESC on `order` field
?>
```

### PublishableTrait (`App\Model\Extension\PublishableTrait`)

The model must have `published_at` attribute with type `DATE` or `DATETIME`.
```
#!php
<?php
    Model::published()->get()   // get all published model
    Model::unpublished()->get() // get all unpublished model

    $model->published = true            // publish model. set published_at attribute to current date.
    $model->published = false           // unpublish model. set published_at attribute to null
    $published = $model->published;     // get whether the model is published or not
?>
```

### TaggableTrait (`App\Model\Extension\TaggableTrait`)

This trait add functionality of having coma-separated tags on model.
```
#!php
<?php
    $model->tags = 'tag1,tag2,tag3' // assign tags

    // save tags by event
    $model->save();                 // automatic added new tag and assign relation
    // save tags on demand
    $model->saveTags('tag1,tag2,tag3');

    $tags = $model->getStringTags()         // get coma-separated tags
?>
```

### SlugableTrait (`App\Model\Extension\SluggableTrait`)

This trait handle slug generation for model which have `slug` attribute. This trait requires `$slug_with` attribute on the class for field name which is used to generate slug.  **Note** don't forget to define `$slug_with` attribute on the class.

```
#!php
<?php
    use App\Model\Extension\SluggableTrait;
    class Model extends BaseModel {
        use SluggableTrait;

        protected $slug_with = 'title';
    }
?>
```

### TreeTrait (`App\Model\Extension\TreeTrait`)

This trait is to handle models or tables which are have relation to themselves.

```
#!php
<?php
    $model->hasChild();                 // return if model has child or not
    $model->getAllChilds(['*'], 3)      // return all childs until depth 3 (root's depth is 0)
    $model->fromRoot()->get();          // return all models that has no parent.
    $model->fromParent(1)->get()        // return all models from parent with key '1'
    $model->notThis(true)->get()        // return all models except current model and its child. set parameter to false if you want without child. The default is false
    $model->withChilds()->get()         // return all models with its child using eagerloading method.
?>
```

### AttachableTrait (`App\Model\Extension\AttachableTrait`)

This Trait handle File upload, local file, url, and youtube url (depends on `App\Supports\Attachment\ManagerFacade` implementation). All download operation will be run when on Model Save, if fail will throw exception. To use this attachment, your model database table migration must have following fields
```
#!php
<?php
    $table->string('field', 500);                       // file path
    $table->tinyInteger('field_type')->default(0);      // to store attachment type, Current only, File (0) atau Youtube (1)
    $table->text('field_info');                         // to store all attachment information such as mime, size, youtube code, embed url, etc.
?>
```

Usage example:

```

#!php
<?php
    use App\Model\Extension\AttachableTrait;

    class Model extends BaseModel {
        use AttachableTrait;

        protected $attachable = [
            'field' => [
                'thumb' => [
                    'small' => '100x100',
                    'full_width' => '_x600',
                    'full_height' => '600x_'
                ]
            ],
            'field2' => []  // if there is no thumbnail
        ];
    }

    $model = new Model;

    $model->field = 'path/to/file';
    //or
    $model->field = 'http://domain/path/to/file';
    //or
    $model->field = 'http://youtube.com/watch?v=code';

    $model->save();

    // Retrieve Attachment data
    $model->getThumbnail('field', 'small'); // for image only. if not image will return default url (by default null)
    $model->getAttachment('field');         // 'http://localhost/files/path/to/file'
    $model->getAttachmentInfo('field', 'mime_type');

    // for youtube
    $model->getAttachmentInfo('field', 'code');         // return youtube code
    $model->getAttachmentInfo('field', 'embed_url');    // return youtube embed url
    $model->getAttachmentInfo('field', 'real_url');     // return youtube real url

?>

```

[Next: BaseForm](https://gitlab.com/suitmedia/suitcms/wikis/BaseForm)
